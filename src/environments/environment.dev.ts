export const environment = {
  production: true,
  url_socket: "ws://localhost:8080/uptoboxSocket",
  url_server: "http://localhost:8080"
};
