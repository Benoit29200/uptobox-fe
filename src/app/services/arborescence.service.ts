import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FichierDto } from '../models/FichierDto';

@Injectable({
  providedIn: 'root'
})
export class ArborescenceService {

  url_server : string;

  constructor(private http: HttpClient) { 
    this.url_server  = environment.url_server;
  }

  getArborescence(niveauLimite : number,viewfileHidden : boolean ) : Observable<FichierDto> {
    return this.http.get<FichierDto>(this.url_server + '/arborescence',{params: {niveauLimite: JSON.stringify(niveauLimite),viewfileHidden: JSON.stringify(viewfileHidden)}});
  }

  
}
