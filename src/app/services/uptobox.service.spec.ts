import { TestBed } from '@angular/core/testing';

import { UptoboxService } from './uptobox.service';

describe('UptoboxService', () => {
  let service: UptoboxService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UptoboxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
