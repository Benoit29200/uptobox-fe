import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment'; 
import { Observable } from 'rxjs';
import { lienInfoDto } from 'src/app/models/lienInfoDto';

@Injectable({
  providedIn: 'root'
})
export class UptoboxService {

  url_server : string;

  constructor(private http: HttpClient) { 
    this.url_server  = environment.url_server;
  }

  getInfoLien(lien : string) : Observable<lienInfoDto> {
    return this.http.get<lienInfoDto>(this.url_server + '/info/lien',{params: {lien: lien}});
  }

  lienIsPresentServer(lien : string) : Observable<boolean> {
    return this.http.get<boolean>(this.url_server + '/info/lienIsPresentServer',{params: {lien: lien}});
  }

  verificationAutorisation(password : string) : Observable<boolean> {
    return this.http.get<boolean>(this.url_server + '/verification/password',{params: {password: password}});
  }

  saveLiens(liens: lienInfoDto[]) : Observable<boolean> {
    return this.http.post<boolean>(this.url_server + '/lien/save',liens);
  }
}