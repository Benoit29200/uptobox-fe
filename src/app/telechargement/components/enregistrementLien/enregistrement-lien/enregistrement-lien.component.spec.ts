import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnregistrementLienComponent } from './enregistrement-lien.component';

describe('EnregistrementLienComponent', () => {
  let component: EnregistrementLienComponent;
  let fixture: ComponentFixture<EnregistrementLienComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnregistrementLienComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnregistrementLienComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
