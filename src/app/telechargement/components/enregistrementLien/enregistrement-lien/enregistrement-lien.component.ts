import {Component, OnInit} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatSnackBar} from '@angular/material/snack-bar';
import { FormGroup, FormControl, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { lienInfoDto } from 'src/app/models/lienInfoDto';
import { UptoboxService } from 'src/app/services/uptobox.service';
import * as cloneDeep from 'lodash/cloneDeep';
import * as _ from 'lodash';
import * as Stomp from 'stompjs';
import { environment } from 'src/environments/environment';
import { ArborescenceService } from 'src/app/services/arborescence.service';
import { FichierDto } from 'src/app/models/FichierDto';



@Component({
  selector: 'app-enregistrement-lien',
  templateUrl: './enregistrement-lien.component.html',
  styleUrls: ['./enregistrement-lien.component.scss']
})
export class EnregistrementLienComponent implements OnInit {

  ws: any;
  uploadForm : FormGroup;
  autorisationDownloadForm : FormGroup;
  submitted : boolean;
  lienTable : lienInfoDto[] = [];
  lienAjout : lienInfoDto;
  _uptoboxService : UptoboxService;
  _arborescenceService : ArborescenceService;
  progressBarValue: number;
  autorisationDowload: boolean;

  arborescence : FichierDto;


  constructor(private fb: FormBuilder, uptoboxService : UptoboxService, private _snackBar: MatSnackBar, arborescenceService: ArborescenceService) {

    this.uploadForm = new FormGroup({
      lien: new FormControl()
   });

   this.uploadForm = this.fb.group({
    lien: ['',[Validators.required,lienUptoboxValidator]]
    });

    this.autorisationDownloadForm = new FormGroup({
      autorisationDownload: new FormControl()
   });

   this.autorisationDownloadForm = this.fb.group({
    autorisationDownload: ['',[Validators.required]]
    });

    this._uptoboxService = uptoboxService;
    this._arborescenceService = arborescenceService;
    this.lienAjout = new lienInfoDto("","",null,1,0);
    this.autorisationDowload = false;

  }

  ngOnInit(){
    this._arborescenceService.getArborescence(2,false).subscribe(
      data => { 
        this.arborescence = data;
      console.log(data)},
      err => console.error(err)
    );
  }

  verificationPassword(){
    if(this.autorisationDownloadForm.invalid){
      return;
    }

    this._uptoboxService.verificationAutorisation(this.autorisationDownloadForm.value.autorisationDownload).subscribe(
      isCorrect => { 
        this.autorisationDownloadForm.controls['autorisationDownload'].setErrors({'autorisationIncorrect': !isCorrect});
          this.autorisationDowload = isCorrect;
      },
      err => console.error(err)
    );

  }

  changeIsUrgent(lien: lienInfoDto){
    if(lien.urgent === 1){
      lien.urgent = 0;
    } else {
      lien.urgent = 1;
    }
  }

  getInfoLien(){

    if(this.uploadForm.invalid){
      return;
    }
    
    if(this.uploadForm.value.lien.lastIndexOf('?') !== -1){
      this.uploadForm.value.lien = this.uploadForm.value.lien.slice(0, this.uploadForm.value.lien.lastIndexOf('?'));
    }
    if(this.uploadForm.value.lien.startsWith('http://')){
      this.uploadForm.value.lien = "https" + this.uploadForm.value.lien.substring(4);
    }

    this._uptoboxService.lienIsPresentServer(this.uploadForm.value.lien).subscribe(
      data => {
        if(data){
          this.uploadForm.controls['lien'].setErrors({'isPresentServer': true});
        }
      },
      err => console.error(err)
    );

    this._uptoboxService.getInfoLien(this.uploadForm.value.lien).subscribe(
      data => { 
        if(data == null){
          this.uploadForm.controls['lien'].setErrors({'nonTrouvee': true});
        }else {
          this.lienAjout = new lienInfoDto(data.lien,data.nom,data.taille,1,this.lienAjout.urgent)
         }},
      err => console.error(err)
    );

    console.log(this.uploadForm);

  }

  addInfoLien(){
    if(this.uploadForm.invalid || this.lienAjout.taille === undefined){
      return;
    }

    let lien : lienInfoDto = cloneDeep(this.lienAjout);
    let isPresent : boolean = false;

    this.lienTable.forEach(function(lienTab){
      if(lienTab.lien === lien.lien){
        isPresent = true;
      }
    });

    if(!isPresent){
      this.lienTable.push(lien);
      this.resetLigneAjout();
    } else {
      this.uploadForm.controls['lien'].setErrors({'isPresent': true});
    }
  }

  saveLiens(){
    // si aucun lien dans le tableau, alors on enregistre rien
    if(_.isEmpty(this.lienTable)){
      return;
    }

    console.log(this.lienTable);

    this._uptoboxService.saveLiens(this.lienTable).subscribe(
      saveOK => { 
        if(saveOK){
          this.lienTable = [];
          this._snackBar.open("Vos liens seront téléchargés !", "Fermer", {
            duration: 6000,
          });
        }else {
          this._snackBar.open("Erreur lors de l'enregistrement des liens", "Fermer", {
            duration: 6000,
          });
        }},
      err => console.error(err)
    );

  }

  removeLien(lien : string){
    let lienTrouve : lienInfoDto;
    this.lienTable.forEach(function(lienTab){
        if(lienTab.lien === lien){
          lienTrouve = lienTab;
        }
    });
    const index = this.lienTable.indexOf(lienTrouve, 0);
          if (index > -1) {
            this.lienTable.splice(index, 1);
          }
  }

  // convenience getter for easy access to form fields
  get f() { return this.uploadForm.controls; }
  // convenience getter for easy access to form fields
  get g() { return this.autorisationDownloadForm.controls; }

  resetLigneAjout(){
    this.uploadForm = this.fb.group({
      lien: ['',[Validators.required,lienUptoboxValidator]]
      });
      this.lienAjout = new lienInfoDto("","",null,1,0);
  }
}


export function lienUptoboxValidator(control: AbstractControl) {
  if (!control.value.startsWith('https://www.uptobox.com/') &&
      !control.value.startsWith('http://www.uptobox.com/') &&
      !control.value.startsWith('www.uptobox.com/') &&
      !control.value.startsWith('http://uptobox.com/')&&
      !control.value.startsWith('https://uptobox.com/')) {
    return { lienUptobox: true };
  }
  return null;
}
