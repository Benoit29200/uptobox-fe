import { Component, OnInit } from '@angular/core';
import * as Stomp from 'stompjs';
import { fichierInfoDto } from 'src/app/models/fichierInfoDto';
import { environment } from 'src/environments/environment'; 

@Component({
  selector: 'app-telechargement',
  templateUrl: './affichageListFilm.html',
  styleUrls: ['./affichageListFilm.scss']
})
export class AffichageListFilmComponent implements OnInit {

  fichiers: fichierInfoDto[] = [];
  showConversation: boolean = false;
  ws: any;
  name: string;
  disabled: boolean;

  constructor(){}

  ngOnInit(){    
  }
  

  connect() {
    let socket = new WebSocket(environment.url_socket);
    this.ws = Stomp.over(socket);
    let that = this;
    this.ws.connect({}, function(frame) {
      that.ws.subscribe("/errors", function(message) {
        alert("Error " + message.body);
      });
      that.ws.subscribe("/uptobox/listFilm/reply", function(message) {
        that.AfficherListeFilmTelecharge(message.body);
      });
      that.disabled = true;
    }, function(error) {
      alert("STOMP error " + error);
    });
  }

  disconnect() {
    if (this.ws != null) {
      this.ws.ws.close();
    }
    this.setConnected(false);
    console.log("Disconnected");
  }

  request() {
    this.ws.send("/app/getListFilmTelecharge", {}, this.name);
  }

  AfficherListeFilmTelecharge(listFichierString : string) {
    this.fichiers = [];
    this.showConversation = true;
    var listFichier : fichierInfoDto[];
    listFichier = JSON.parse(listFichierString); 
    for (let fichier of listFichier) {
      this.fichiers.push(fichier);
    }
  }

  setConnected(connected) {
    this.disabled = connected;
    this.showConversation = connected;
    this.fichiers = [];
  }
}
