import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffichageListFilmComponent } from './affichageListFilm.component';

describe('TelechargementComponent', () => {
  let component: AffichageListFilmComponent;
  let fixture: ComponentFixture<AffichageListFilmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffichageListFilmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffichageListFilmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
