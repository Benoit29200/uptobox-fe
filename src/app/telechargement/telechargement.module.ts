import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from '../app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AffichageListFilmComponent } from './components/affichageListFilm/affichageListFilm.component';
import { EnregistrementLienComponent } from './components/enregistrementLien/enregistrement-lien/enregistrement-lien.component';

import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatCardModule} from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';
import {MatProgressBarModule} from '@angular/material/progress-bar'

@NgModule({
  declarations: [AffichageListFilmComponent, EnregistrementLienComponent],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatInputModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatCardModule, MatButtonModule, MatTableModule
  ],
  exports: [AffichageListFilmComponent, EnregistrementLienComponent],
})
export class TelechargementModule { }
