import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TelechargementService {

  constructor(protected http: HttpClient) { }

  getData() {
    return this.http.get<String>('http://192.168.0.23:8080/uptobox-be/get?name=mon');
  }
}
