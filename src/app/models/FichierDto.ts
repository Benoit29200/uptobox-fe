import { SizeDto } from './SizeDto';

export class FichierDto {
    name : String;

    sizeInBytes : number;
    size : SizeDto;

    niveau : number;

    isDirectory : boolean;
    hasChildren : boolean;
    isHide : boolean;
    isReadable : boolean;
    isWritable : boolean;
    isExecutable : boolean;

    dateCreation : Date;
    dateModification : Date;
    dateDernierAccess : Date;

    childrens : FichierDto[];

    constructor(){
    }
    
}