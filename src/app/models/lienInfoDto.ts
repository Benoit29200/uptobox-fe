export class lienInfoDto {
    lien: string;
    nom : string;
    taille : string;
    etat: number;
    urgent: number;

    constructor(lien : string, nom : string, taille : string, etat: number,urgent: number ){
        this.lien = lien;
        this.nom = nom;
        this.taille = taille;
        this.etat = etat;
        this.urgent = urgent;
    }
    
}