export class fichierInfoDto {
    nom : string;
    taille : number;
    unite : string;
    tailleSecondaire : number;
    uniteSecondaire : string;
}